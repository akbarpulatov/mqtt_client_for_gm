#include <SPI.h>
#include <Ethernet.h>
#include <PubSubClient.h>

#define USERBUTTON1 6
#define USERBUTTON2 3
#define USERBUTTON3 2

#define ALARMRED    9
#define ALARMYELLOW 8
#define ALARMGREEN  7

//#define digitalRead(n)  

unsigned char ucData[3];


// Function prototypes
void subscribeReceive(char* topic, byte* payload, unsigned int length);
 
// Set your MAC address and IP address here
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
IPAddress ip(192, 168, 1, 160);
 
// Make sure to leave out the http and slashes!
const char* server = "test.mosquitto.org";
 
// Ethernet and MQTT related objects
EthernetClient ethClient;
PubSubClient mqttClient(ethClient);

void setup()
{
  pinMode(USERBUTTON1, INPUT);
  pinMode(USERBUTTON2, INPUT);
  pinMode(USERBUTTON3, INPUT);
  
  pinMode(ALARMRED, INPUT);
  pinMode(ALARMYELLOW, INPUT);
  pinMode(ALARMGREEN, INPUT);
  
  // Useful for debugging purposes
  Serial.begin(9600);
  
  // Start the ethernet connection
  Ethernet.begin(mac, ip);              
  
  // Ethernet takes some time to boot!
  delay(3000);                          
 
  // Set the MQTT server to the server stated above ^
  mqttClient.setServer(server, 1883);   
 
  // Attempt to connect to the server with the ID "myClientID"
  if (mqttClient.connect("myClientID")) 
  {
    Serial.println("Connection has been established, well done");
 
    // Establish the subscribe event
    mqttClient.setCallback(subscribeReceive);
  } 
  else 
  {
    Serial.println("Looks like the server connection failed...");
  }
}


void loop()
{
  // This is needed at the top of the loop!
  mqttClient.loop();
 
  // Ensure that we are subscribed to the topic "MakerIOTopic"
  mqttClient.subscribe("Islomxuja");
  
  //Reads inputs:alarms and buttons


  ucData[0] = '0'+ (unsigned char)(((!digitalRead(ALARMRED)) << 2) | 
                                   ((!digitalRead(ALARMYELLOW)) << 1) |
                                   (!digitalRead(ALARMGREEN)));

  ucData[1] ='0'+ (unsigned char)(((!digitalRead(USERBUTTON1)) << 2) | 
                                  ((!digitalRead(USERBUTTON2)) << 1) |
                                  (!digitalRead(USERBUTTON3)));
  ucData[2] = 0;                          


  // Attempt to publish a value to the topic "MakerIOTopic"
  if(mqttClient.publish("Islomxuja", ucData))
  {
    Serial.println("Publish message success");
  }
  else
  {
    Serial.println("Could not send message :(");
  }

  // Dont overload the server!
  delay(1000);
}

void subscribeReceive(char* topic, byte* payload, unsigned int length)
{
  // Print the topic
  Serial.print("Topic: ");
  Serial.println(topic);
 
  // Print the message
  Serial.print("Message: ");
  for(int i = 0; i < length; i ++)
  {
    Serial.print(char(payload[i]));
  }
 
  // Print a newline
  Serial.println("");
}
